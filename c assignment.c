#include<stdio.h>

struct student
{
    char FirstName[20];
    char Subject[10];
    int marks;
};

int main()
{
    struct student info[5];
    for(int i=0; i<sizeof(info)/sizeof(struct student); i++)
    {

        printf("Enter first name:");
	    scanf(" %[^\n]s",&info[i].FirstName);
	    printf("Enter subject name:");
	    scanf(" %[^\n]%*c",&info[i].Subject);
	    printf("Enter subject marks:");
	    scanf("%d",&info[i].marks);
    }
    printf("---------------------------\n");
    for(int i=0; i<sizeof(info)/sizeof(struct student); i++)
    {
        printf("info of student- %d\n",i+1);
        printf("%s\n",info[i].FirstName);
        printf("%s\n",info[i].Subject);
        printf("%d\n",info[i].marks);
        printf("---------------------------\n");
    }
    return 0;
}

